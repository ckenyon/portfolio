Django==1.11
twilio==6.0.0
psycopg2==2.7.3
urllib3==1.22
gunicorn==20.1.*
