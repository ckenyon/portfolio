from django.core.management.base import BaseCommand, CommandError
from django.template.loader import render_to_string
import pdfkit
import os

class Command(BaseCommand):
    help = 'Generate resume from the html'

    def handle(self, *args, **options):
        html = render_to_string("portfolio/resume.html", {"printable":True})
        html.strip()
        to_path = os.getcwd() + '/portfolio/static/portfolio/docs/'
        name  = 'Resume.pdf'
        options = {
            'page-size': 'Letter',
            'margin-top': '0in',
            'margin-right': '0in',
            'margin-bottom': '0in',
            'margin-left': '0in',
            'enable-smart-shrinking': True,
        }
        pdfkit.from_string(html, to_path+name, options=options)
