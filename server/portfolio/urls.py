from django.urls import re_path
from django.conf.urls import handler404
from . import views

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^message$', views.message, name='msg'),
    re_path(r'^projects/$', views.projects, name='projects'),
    re_path(r'^resume_click$',views.resume_click,name='resume_click'),
    re_path(r'^resume/$',views.resume,name='resume'),
]

handler404 = views.not_found
