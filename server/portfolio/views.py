import django
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
#from portfolio.models import *
from urllib.request import urlopen
import json
from twilio.rest import Client
account_sid = "AC6cd9aa23b9e4ffa073c95027ca448e9b" # Your Account SID from www.twilio.com/console
auth_token  = "53d43796bd60d07422d9a740bfcd1c93"  # Your Auth Token from www.twilio.com/console
client = Client(account_sid, auth_token)

#  Responses:
SUCCESS = "Success!"
FAILURE = "Failure!"


def not_found(request, exception):
    '''
        Return the 404 Page not found page when an invalid route is used
    '''
    context = {}
    return render(request,'portfolio/404.html',status=404)

def index(request):
    '''
        Return the main page
    '''
    context = {'DEBUG_MODE' : django.conf.settings.DEBUG}
    return render(request, 'portfolio/index.html')

def get_client_ip(request):
    '''
        A helper function to extract the client ID from the request object
    '''
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def projects(request):
    '''
        Return the detailed projects page
        Track visits to projects page to log in the DB when a download is done
    '''
    context = {'DEBUG_MODE' : django.conf.settings.DEBUG}
    '''
    try:
        ip = get_client_ip(request)
        res = urlopen('http://freegeoip.net/json/{}'.format(ip)).read().decode().strip()
        geo_data = json.loads(res)
        ProjectClick(date=timezone.now(), ip=ip,
                     lng=float(geo_data['longitude']),
                     lat=float(geo_data['latitude']),
                     city=geo_data['city']).save()
    except Exception as e:
        pass
    '''
    return render(request, 'portfolio/projects.html',context)

def resume_click(request):
    '''
        Track resume clicks to log in the DB when a download is done
        If you're reading this, yes, I have you logged muhuhahahaha
    '''
    '''
    try:
        ip = get_client_ip(request)
        res = urlopen('http://freegeoip.net/json/{}'.format(ip)).read().decode().strip()
        geo_data = json.loads(res)
        ResumeClick(date=timezone.now(), ip=ip,
                     lng=float(geo_data['longitude']),
                     lat=float(geo_data['latitude']),
                     city=geo_data['city']).save()
        print("Saved resume click")
    except Exception as e:
        return HttpResponse(FAILURE)
    '''
    return HttpResponse(SUCCESS)

def message(request):
    '''
        Handle a message from the message box and log it in the DB
        Send a Twilio SMS to my phone
    '''
    '''
    name = request.POST.get("name")
    phone = request.POST.get("phone")
    email = request.POST.get("email")
    msg = request.POST.get("message")
    if name and (phone or email) and msg:
        message = Message(name=name, phone=phone, email=email, message=msg)
        message.save()
        response = HttpResponse(SUCCESS)
    else:
        response = HttpResponse(FAILURE)

    try:
        text = You have a message from {} on your website.\
                \nThe message is:\n{}
                \nYou can respond at {} or {}..format(name, msg, email, phone)
        twil_msg = client.messages.create(body=text,
            to="+16039656571",
            from_="+17076403040")
        print("Sending message to Chris:", text)
        print("Twilio SID:",twil_msg.sid)
    except Exception as e:
        print(e)
    '''

    return response

def resume(request):
    return render(request, 'portfolio/resume.html', {"printable":False})
