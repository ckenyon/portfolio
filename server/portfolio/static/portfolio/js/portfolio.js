function resetSubmit() {
    $("#success-alert").hide("slow")
    $("#submit-btn").fadeTo(500,1)
};

$('#contact_form').submit(function(e) {
    e.preventDefault()
    $.post($(this).attr('action'), $(this).serialize(), function(response){
        console.log("Sending. This will notify Chris via SMS.")
    },'json');
    $("#submit-btn").fadeTo(500,0)
    $("#success-alert").show("slow")
    setTimeout(resetSubmit, 5000);
    return false;
});

$("#resume_link").click(function(){
    var csrf = $(this).data('csrf');
    $.post("resume_click", {csrfmiddlewaretoken:csrf}, function(data){});}
);

$(document).ready(function() {
    setTimeout(function() {
        $("body").addClass("loaded");
    }, 2000);
});
