from django.db import models

class Message(models.Model):
    name = models.CharField(max_length=255)
    email = models.CharField(null=True,max_length=255)
    phone = models.CharField(null=True,max_length=20)
    message = models.TextField()

class ResumeClick(models.Model):
    date = models.DateTimeField()
    ip = models.CharField(max_length=50)
    lat = models.DecimalField(max_digits=8,decimal_places=5)
    lng = models.DecimalField(max_digits=8,decimal_places=5)
    city = models.CharField(max_length=255)

class ProjectClick(models.Model):
    date = models.DateTimeField()
    ip = models.CharField(max_length=50)
    lat = models.DecimalField(max_digits=8,decimal_places=5)
    lng = models.DecimalField(max_digits=8,decimal_places=5)
    city = models.CharField(max_length=255)
